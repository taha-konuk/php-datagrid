<?php
	namespace knk\datagrid;

	class DataGridHeader
	{
		public static $ClassName = "cell";
		public $content = "";
		public $id = "";

		public function __construct($content)
		{
			$this->content = $content;
		}

		public function toHtml()
		{
			return "<th id=\"".$this->id."\" class=\"".DataGridHeader::$ClassName."\">".$this->content."</th>";
		}
	};
?>
