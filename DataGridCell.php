<?php
	namespace knk\datagrid;
	
	class DataGridCell
	{
		public static $ClassName = "cell";
		public $content = "";
		public $id = "";

		public function __construct($content)
		{
			$this->content = $content;
		}

		public function toHtml()
		{
			return "<td id=\"".$this->id."\" class=\"".DataGridCell::$ClassName."\">{$this->content}</td>";
		}
	};
?>
