<?php
	namespace knk\datagrid;

	require_once("DataGridRow.php");
	require_once("DataGridHeader.php");

	class DataGrid
	{
		public static $ClassName = "datagrid";
		public $id = "";

		private $rows = array();
		private $headers = array();

		public function __construct()
		{

		}
		public function addRow(DataGridRow $row)
		{
			$this->rows[] = $row;
		}

		public function addHeader(DataGridHeader ...$headers)
		{
			foreach($headers as $header)
				$this->headers[] = $header;
		}

		public function toHtml()
		{
			$html = "<table id=\"".$this->id."\" class =\"".DataGrid::$ClassName."\">";

			foreach($this->headers as $header)
			{
				$html .= $header->toHtml();
			}

			foreach($this->rows as $row)
			{
				$html .= $row->toHtml();
			}

			$html .= "</table>";
			
			return $html;
		}

		public function show()
		{
			echo $this->toHtml();
		}
	};
?>
