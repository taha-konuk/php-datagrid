<?php
	namespace knk\datagrid;

	require_once("DataGridCell.php");

	class DataGridRow
	{
		public static $ClassName = "data-grid-row";
		public $id = "";

		private $cells = array();

		function __construct(String ...$cells)
		{
			foreach($cells as $content)
			{
				$cell = new DataGridCell($content);
				$this->addCell($cell);
			}
		}
		
		public function addCell(DataGridCell $cell)
		{
			$this->cells[] = $cell;
		}

		public function removeCell(number $index)
		{
			unset($this->cells[$index]);
		}

		public function toHtml()
		{
			$html = "<tr id=\"".$this->id."\" class=\"".DataGridRow::$ClassName."\">";
			
			foreach($this->cells as $cell)
			{
				$html .= $cell->toHtml();
			}

			$html .= "</tr>";
			
			return $html;
		}
	};
?>
