<?php
	namespace knk\datagrid;

	require_once("DataGrid.php");
	require_once("DataGridRow.php");

	$header1 = new DataGridHeader("Column 1");
	$header2 = new DataGridHeader("Column 2");
	$header3 = new DataGridHeader("Column 3");
	$header4 = new DataGridHeader("Column 4");

	$link = '<a href="?p=asd">details</a>';
	$row1 = new DataGridRow("asdasdasd","asdasd","adsasdgd","fdgdfh","<button>sonradan ekleme</button>");
	$row2 = new DataGridRow("ahsdasdasd","asdasd","adsasdgd","fdgdfh");
	$row3 = new DataGridRow("asdaksdasd","asdasd","adsasdgd","fdgdfh",$link);
	$row4 = new DataGridRow("asdjasdasd","asdasd","adsasdgd","fdgdfh");
	
	$dataGrid = new DataGrid();
	
	$dataGrid->addHeader($header1);
	$dataGrid->addHeader($header2);
	$dataGrid->addHeader($header3);
	$dataGrid->addHeader($header4);

	$dataGrid->addRow($row1);
	$dataGrid->addRow($row2);
	$dataGrid->addRow($row3);
	$dataGrid->addRow($row4);
?>

<html>
	<head>
		<style>
			.datagrid
			{
				border-collapse: collapse;
			}
			td, th
			{
				border: 1px solid #dddddd;
	    		text-align: left;
	    		padding: 8px;
	    	}
			tr:nth-child(even)
			{
				background-color: #dddddd;
			}
	    </style>
	</head>
	<body>
		<?php $dataGrid->show(); ?>
	</body>
</html>
